function result = RunExperiment ( doTrial, ex, params, blockStart )
% The body of the experiment: RunExperiment (@doTrial, ex, params [, @blockStart])
% (c) Sanjay Manohar 2008
%
% What It Does:
% ------------
% 1) Create screen (type 'help prepareScreen' for details)
%
% 2) Initialise eyelink (if ex.useEyelink==1), recording default eye
%    position information to a date-numbered file. (if useEyelink==2 then
%    use a dummy eyelink setup)
%
% 3) Combine information from 'ex' and 'params' structures. Params is 
%    either interpreted as values that override those of ex, or alternatively
%    if params is the result from a previous experiment, then simply continue
%    the previous experiment.
%    If not continuing a previous experiment, create trial structure 
%    (type help createTrials) using parameters in ex.
% 
% 4) Iteratively call the given trial function: doTrial(scr, el, ex, trial) 
%    Parameters sent: screen structure, eyelink structure, 
%      experiment parameters structure, and trial parameters structure
%
% 5) Handles eyelink trial, calibration, drift correction, and abort logic.
%
% 6) Handles keypresses 
%      'R'    repeat trial now
%      'D'    drift correct
%      'C'    tracker setup screen
%      'F1'   allow keyboard input to modift experiment
%      escape exit block and exit experiment. 
%    The key result should be passed
%    back from doTrial in the return structure as trial.key, and trial.R
%    should return a status (R_ERROR terminates block).
% 
% 7) Handles errors: saves all data in 'errordump.mat', cleans screen and
%    shuts down eyelink; also attempts to upload EDF file to current
%    directory. Also, data is saved at every block-end, in LastExperiment.dat
%
% NOTE:
%  1) ex.R_ERROR (any number) and ex.useEyelink (true/false) must be defined.
%  2) doTrial must be a function handle (passed using @) with the
%     parameters as described in 4 above
%
% functions:        
%                   
% doTrial:          user-supplied function of the form 
%                   trialResult = doTrial(scr, el, ex, trial)
% params:           Either - additional experimental parameters which 
%                   override those in 'ex', or 'result' returned from
%                   a previous RunExperiment.
%                   set a field 'overrideParameters' if you want to
%                   automatically override any parameters set in the
%                   experiment program, otherwise you will be asked.
%
% ex:
%  practiceTrials   = number of practice trials
%  blocks           = number of blocks, or alternatively an array of 
%                     integers specifying the block types for each block to
%                     run. Default = 1
%  blockLen         = number of trials per block
%  blockVariables.varName1  = [value1, value2, value3]
%  trialVariables.varName2  = [value4, value5]
%  useEyelink       = whether to use eyelink
%  useScreen        = whether to use the PsychToolbox Screen command
%  R_NEEDS_REPEATING
%  R_ERROR          = arbitrary constants given as results in tr.R
%  R_ESCAPE
%
% blockStart:       user supplied function of the form 
%                   blockStart (scr, el, ex, trial), where the trial
%                   supplied is the first trial of the block. use this to
%                   write a display needed at the start of a block.
%
%
% result:
%  file             = EDF file name for eyelink data (hopefully transferred
%                     into local working directory at end of experiment)
%  trials(block,trial) = trial parameters structure - the specific 
%                     parameters created by createTrials, and sent 
%                     sequentially to each doTrial.
%  params           = The experimental parameters, combined
%  data(block,trial)= The results of each trial, as returned by doTrial.
%                     Once you have discarded unwanted trials/reordered the
%                     trials for analysis, you can use
%                     transpIndex(result.data) to access the values as
%                     matrices.
%  practiceResult(i)= results returned from doTrial for the practice
%                     trials.
%  last (1x2 double)= last block and trial that was successfully completed;
%                     this is the point to continue from if 'result' is
%                     used as the 'params' input to RunTrials.
%  date             = date/time string of start of experiment.



% setup parameters

% values of responseType, supplied by doTrial.
% send these values in tr.R to indicate the outcome of each trial
ex.R_ERROR        = -99;            % trial was an error; leave it and do nothing
ex.R_NEEDS_REPEATING = -97;         % error: rerun trial immediately after
ex.R_NEEDS_REPEATING_LATER = -95;   % error: rerun trial at end of block
ex.R_ESCAPE       = -98;            % escape was pressed - exit immediately
ex.R_INCOMPLETE   = -96;            % trial didn't complete as expected
ex.R_UNSPECIFIED  = -94;            % experiment didn't provide a return value


last = [1 1];                       % block and trial to start at
if(exist('params','var'))           % if user specified a set of experimental 
    p=params;                       % parameters in the input parameters,
    fnames=fieldnames(p);           % override parameters from the original 
    for x=[1:size(fnames)];         % with those from the input structure
        if isfield(ex,fnames{x}) if ~equals(p.(fnames{x}),ex.(fnames{x}))
            if(isfield(p,'overrideParameters')) % if 'overrideParameters' is set, then use that value
                override=p.overrideParameters; 
                if(override)        % overwrite all parameters if 'overrideParameters' is specified
                    warning(['Using "' fnames{x} '" = "' p.(fnames{x}) '" from passed parameters.']);
                else
                    warning(['Using "' fnames{x} '" = "' ex.(fnames{x}) '" from experiment program.']);
                end
            else                    % otherwise ask specifically for each parameter
                override=(input(['Override "' fnames{x} '": "' p.(fnames{x}) ...
                            '" instead of "' ex.(fnames{x}) '" (1/0) ?'] ))
            end;
        end;end;
        ex=setfield(ex, fnames{x}, getfield(p, fnames{x}));
    end;
    if isfield(params, 'last')  last=params.last; end;      % go straight to the last-executed trial 
    if isfield(params,'data')   results=params.data; end;   % keep results of old trials
    if isfield(params,'trials') trials=params.trials; end;  % keep old trial structure and randomisation 
    if isfield(params,'edfFiles')                          
        result.edfFiles=params.edfFiles;  % keep a list of EDF files used in previous runs
    else
        result.edfFiles={};
    end;
    if isfield(params,'startTimes')       % keep a list of times that each run begins
        result.startTimes=params.startTimes;
    else
        result.startTimes={};
    end;
    usingoldresults=1;
else usingoldresults=0; end;

if ~isfield(ex,'useEyelink') ex.useEyelink=0;end;  % default No Eyelink
if ~isfield(ex,'useScreen') ex.useScreen=1;end;    % default Yes Screen
if ex.useEyelink, ex.useScreen=1;end;              % can't have eyelink without screen
if ~ex.useScreen, scr=0;end;
if ~ex.useEyelink  el=0;end;
if(~exist('result','var')) result.startTimes={};end;
if(~isfield(ex,'blocks')) warning('Assuming 1 block only'); ex.blocks=1;end;
result.startTimes   = {result.startTimes{:}, datestr(now,31)}; % store time of experiment start
    
%%%%%%%%%%% CREATE TRIALS %%%%%%%%%%%%%%%%%
if exist('trials')~=1,  trials = createTrials(ex); end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


try                       
    if ex.useScreen
        scr=prepareScreen(ex);         % initialise screen (scr struct)
        ex.screenSize=scr.ssz;  
    end;

    datestring=datestr(now,30);
    namebase=datestring([5:8, 10:13]); % base filename = date
    if(ex.useEyelink)                  % initialise eyelink (el struct)
        if eyelink('IsConnected') eyelink('Shutdown');end;
        if(ex.useEyelink==1)
            eyelink('initialize');
        else
            eyelink('InitializeDummy');
        end                            % set up calibration, record & receive gaze + pupil
        eyelink('command', 'calibration_type = HV9');
        eyelink('command', 'saccade_velocity_threshold = 35');
        eyelink('command', 'saccade_acceleration_threshold = 9500');
        eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
        eyelink('command', 'file_sample_data = GAZE,AREA,STATUS');
        eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON');
        eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,AREA');
        eyelink('command', 'screen_pixel_coords = 0,0,%d,%d', scr.ssz(1), scr.ssz(2));
        Eyelink('Command', 'pupil_size_diameter = YES');
        el=EyelinkInitDefaults(scr.w);
        el.file=[namebase '.edf'];     
        eyelink('openfile', el.file ); % create the EDF file
        result.file   = el.file;       
        el.disallowEarlySaccades=1;    % store the filename in output
        el.backgroundcolour=ex.bgColour;
        el.foregroundcolour=ex.fgColour;
        if(~isfield(result,'edfFiles')) result.edfFiles={};end;
        result.edfFiles={result.edfFiles{:}, result.file};
        el.callback=[];
        EyelinkDoTrackerSetup(el);     % run the calibration routine
        FlushEvents;                   
    end;
    result.trials = trials;     % save trial structure in output
    result.params = ex;         % save experimental parameters in output
    fatal_error   = 0; 

    % Practice trials
    if isfield(ex,'practiceTrials') & prod(last)==1
        prac=createTrials(ex);
        if ex.useScreen
            drawTextCentred(scr, 'Practice trials', ex.fgColour);
            screen('Flip', scr.w);
        end;
        KbWait; 
        for i=1:ex.practiceTrials
          tr = runSingleTrialAndProcess(scr, el, ex, trials(1+floor(i/ex.blockLen),1+mod(i,ex.blockLen)),doTrial,0,i);
          result.practiceResult(i)=tr;
          if (tr.R==ex.R_ERROR | tr.R==ex.R_ESCAPE) fatal_error=1; break; end;
        end
    end;
            
            

    %%%%%%%%%%%%%%%%% Main blocks and trials %%%%%%%%%%%%%%%%%
    if ex.useScreen
        screen(scr.w, 'FillRect',ex.bgColour, scr.sszrect);
        drawTextCentred(scr, 'Start of Experiment', ex.fgColour);
        screen('Flip', scr.w);
    else
        disp('Start of experiment - press a key');
    end;
    KbWait;               % press a key to start the experiment
    
    
    if length(ex.blocks)==1, bnum=last(1):ex.blocks;
    else bnum = ex.blocks(last(1):end);
    end;                  % continue from last block
    for b=last(1):ex.blocks
        if exist('blockStart')    % call the blockStart method if supplied
            kcode = 1; while any(kcode) [z z kcode]=KbCheck; end;
            FlushEvents '';
            blockStart(scr,el,ex,trials(b,1));
        end;
        repeatLater = []; % trials to repeat at end of block
        for t=1:ex.blockLen
            if(b==last(1) & t<last(2)) continue; end; % skip through trials already done
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Run single trial
            tr=runSingleTrialAndProcess(scr,el,ex,trials(b,t),doTrial,b,t);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if(exist('results')==1)  % to append previous struct, ensure all the trials have the same data fields
                [results tr]=ensureStructsAssignable(results,tr);
            else results=[]; % (first trial)
            end;
            results = [results tr];  % then append
            if(tr.R==ex.R_ERROR | tr.R==ex.R_ESCAPE) fatal_error=1; break; end;
            if(isfield(ex,'R_NEEDS_REPEATING_LATER') && tr.R==ex.R_NEEDS_REPEATING_LATER)
              repeatLater=[repeatLater, t]; 
              fprintf('Blk %d Trial %d will be repeated at end\n',b,t);
            end;
            result.data   = results;      % store the results on the output
            result.last   = [b,t];        % keep track of what the last complete trial is
            % write the data to disc in a temporary file after every trial!
            % this allows data to be recovered in from this file after a fatal crash
            save 'LastExperiment' result  
        end; %end of block
        % repeat-later trials... keep going until they don't need repeating.
        % add them to the end of the data, but put in the appropriate trial
        % index for where it would have been in the sequence.
        t=1;while (~fatal_error) && (t<=length(repeatLater)) 
            tr=runSingleTrialAndProcess(scr,el,ex,trials(b,repeatLater(t)),doTrial,b,repeatLater(t));
            tr.trialIndex=repeatLater(t);   % make the trial index the same as it should have been
            tr.isRepeated=1;                % new flag to signify repeated trials
            [results tr]=ensureStructsAssignable(results,tr);
            results = [results tr];
            if(tr.R==ex.R_ERROR | tr.R==ex.R_ESCAPE) fatal_error=1; break; end;
            % allow the trial to repeated more than once
            if(tr.R==ex.R_NEEDS_REPEATING_LATER) repeatLater=[repeatLater, repeatLater(t)]; end; 
            result.data   = results;
            save 'LastExperiment' result
            t=t+1;
        end
        % at the end each block (or when quit), save all trials with the full filename
        save([namebase '.mat'], 'result'); 
        if(~exist('blockStart'))
          if ex.useScreen 
            drawTextCentred(scr, 'End of block', ex.fgColour);
            screen('Flip', scr.w);
          end;
          KbWait;                             % wait for keypress after each block
        end
        [z z kcode]=KbCheck;
        if kcode(27) | fatal_error  break;end;
    end;
    %%%%%%%%%%%%%  end of experiment %%%%%%%%%%%%%%%%%
catch                       % in case of an error
    e=lasterror;            % display error message
    fprintf('Error : %s\n',...
        e.message);
    for(i=1:length(e.stack))
        disp(e.stack(i));
    save 'errordump';
    if exist('results','var')
        result.data=results;% and still give back the data so far
    end;
    end
end

if(ex.useScreen)        
  screen closeall;      % restore screen
end                     
if ex.useEyelink        % close eye data file then transfer
    if(eyelink('isConnected'))
        eyelink('closefile');  
        fprintf('Downloading edf...');
        if eyelink('receivefile',el.file,el.file) < 0
            fprintf('Error in receiveing file!\n'); 
        end;
        fprintf('Done\n');
        eyelink('shutdown');
    end;
end;
FlushEvents '';
ShowCursor;             % show the mouse cursor again



%%%%%%%%% EXIT HERE %%%%%%%%%





function allowinput(scr,ex)
% this allows the user to type at the keyboard and invoke commands.
screen closeall
%screen('OpenWindow', scr.w, ex.bgColour, [0 0 100,100]);
disp('type "return" to return to experiment, or "dbquit" to end expt');
keyboard();
%Screen('OpenWindow', scr.w, ex.bgColour, [0 0 scr.ssz]);



function tr=runSingleTrialAndProcess(scr,el,ex,tr,doTrial,b,t)
% this prepares a trial structure for the experiment,
% calls "doTrial" of your experiment, 
% checks if it needs repeating, and checks for things like calibration
% requests or if the eyelink computer 'abort' or 'end' was pressed.
    tr.block=b; tr.trialIndex=t;         % initialise trial position
    tr.key=[];  tr.R=ex.R_NEEDS_REPEATING;    % initialise trial exit status
    while (tr.R==ex.R_NEEDS_REPEATING)   % repeat trial immediately?
        tr.R=ex.R_INCOMPLETE;
        kcode = 1; while any(kcode) [z z kcode]=KbCheck; end;
        FlushEvents;             % ensure no keys pressed at start of trial
        if ex.useEyelink         %%%% begin recording
            eyelink('startrecording'); 
            el.eye=eyelink('eyeavailable')+1;
            if ~el.eye 
                error('No eye available');
                el.eye=1;
            end;
            eyelink('message', 'B %d T %d', b,t);
        end;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Call External doTrial routine
        tr = doTrial(scr, el, ex, tr);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Returns the results of that trial
        if(~isfield(tr,'key')) tr.key=[]; end            % well, it's just possible that you return a completely
        if(~isfield(tr,'R')) tr.R=ex.R_UNSPECIFIED; end; % different structure to the one we provided!
        [z z kcode]=KbCheck;
        if kcode(27) tr.key=27;end; % override repeat trial if escape pressed.
        if kcode(112) allowinput(scr,ex); end; % f1: allow modification of expt params
        if(length(tr.key)>1) tr.key=tr.key(1);end; 
        if (isempty(tr.key) || (tr.key==0));  tr.key=find(kcode); % expt provided no keypress data --> check our own
        else
          if tr.key=='R' tr.R=ex.R_NEEDS_REPEATING; end;
          if (tr.key=='D' && ex.useEyelink) dodriftcorrection(el ); tr.R=ex.R_NEEDS_REPEATING;end;
          if (tr.key=='C' && ex.useEyelink) EyelinkDotrackersetup(el); tr.R=ex.R_NEEDS_REPEATING; end;
          if tr.key==27  tr.R=ex.R_ESCAPE;  end;
        end
        if ex.useEyelink            %%%% stop recording
            if(tr.R==ex.R_NEEDS_REPEATING) eyelink('message', 'VOID_TRIAL'); end;
            eyelink('stoprecording'); 
            eyeStatus=eyelink('checkrecording');
            switch(eyeStatus)       % check eyelink status - was the trial aborted?
                case el.ABORT_EXPT,   tr.R=ex.R_ESCAPE; eyelink('stoprecording'); break;
                case el.REPEAT_TRIAL, tr.R=ex.R_NEEDS_REPEATING; tr.key='R';eyelink('stoprecording');
                case el.SKIP_TRIAL,   tr.R=ex.R_NEEDS_REPEATING; tr.key='R';eyelink('stoprecording');
            end;
        end;
    end; % while trial needs repeating

