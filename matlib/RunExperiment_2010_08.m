function result = RunExperiment ( doTrial, ex, params, blockStart )
% The body of the experiment: RunExperiment (@doTrial, ex, params [, @blockStart])
% (c) Sanjay Manohar 2008
%
% What It Does:
% ------------
% 1) Create screen (type 'help prepareScreen' for details)
%
% 2) Initialise eyelink (if ex.useEyelink==1), recording default eye
%    position information to a date-numbered file. (if useEyelink==2 then
%    use a dummy eyelink setup)
%
% 3) Combine information from 'ex' and 'params' structures. Params is 
%    either interpreted as values that override those of ex, or alternatively
%    if params is the result from a previous experiment, then simply continue
%    the previous experiment.
%    If not continuing a previous experiment, create trial structure 
%    (type help createTrials) using parameters in ex.
% 
% 4) Iteratively call the given trial function: doTrial(scr, el, ex, trial) 
%    Parameters sent: screen structure, eyelink structure, 
%      experiment parameters structure, and trial parameters structure
%
% 5) Handles eyelink trial, calibration, drift correction, and abort logic.
%
% 6) Handles keypresses 'R' (repeat trial), 'D' (drift correct) and
%    escape (exit block and exit experiment). The key result should be passed
%    back from doTrial in the return structure as trial.key, and trial.R
%    should return a status (R_ERROR terminates block).
% 
% 7) Handles errors: saves all data in 'errordump.mat', cleans screen and
%    shuts down eyelink; also attempts to upload EDF file to current
%    directory. Also, data is saved at every block-end, in LastExperiment.dat
%
% NOTE:
%  1) ex.R_ERROR (any number) and ex.useEyelink (true/false) must be defined.
%  2) doTrial must be a function handle (passed using @) with the
%     parameters as described in 4 above
%
% doTrial:          user-supplied function of the form 
%                   trialResult = doTrial(scr, el, ex, trial)
% params:           Either - additional experimental parameters which 
%                   override those in 'ex', or 'result' returned from
%                   a previous RunExperiment.
%                   set a field 'overrideParameters' if you want to
%                   automatically override any parameters set in the
%                   experiment program, otherwise you will be asked.
%
% ex:
%  practiceTrials   = number of practice trials
%  blocks           = number of blocks, or alternatively an array of 
%                     integers specifying the block types for each block to
%                     run.
%  blockLen         = number of trials per block
%  blockVariables.varName1  = [value1, value2, value3]
%  trialVariables.varName2  = [value4, value5]
%  useEyelink       = whether to use eyelink
%  useScreen        = whether to use the PsychToolbox Screen command
%  R_NEEDS_REPEATING
%  R_ERROR          = arbitrary constants given as results in tr.R
%  R_ESCAPE
%
% blockStart:       user supplied function of the form 
%                   blockStart (scr, el, ex, trial), where the trial
%                   supplied is the first trial of the block. use this to
%                   write a display needed at the start of a block.
%
%
% result:
%  file             = EDF file name for eyelink data (hopefully transferred
%                     into local working directory at end of experiment)
%  trials(block,trial) = trial parameters structure - the specific 
%                     parameters created by createTrials, and sent 
%                     sequentially to each doTrial.
%  params           = The experimental parameters, combined
%  data(block,trial)= The results of each trial, as returned by doTrial.
%                     Once you have discarded unwanted trials/reordered the
%                     trials for analysis, you can use
%                     transpIndex(result.data) to access the values as
%                     matrices.
%  practiceResult(i)= results returned from doTrial for the practice
%                     trials.
%  last (1x2 double)= last block and trial that was successfully completed;
%                     this is the point to continue from if 'result' is
%                     used as the 'params' input to RunTrials.
%  date             = date/time string of start of experiment.



% setup parameters
last = [1 1];               % block and trial to start at
if(exist('params'))         % override parameters from input structure
    p=params;               
    else usingoldresults=0; end;
    fnames=fieldnames(p);
    for x=[1:size(fnames)];
        if isfield(ex,fnames{x}) if ~equals(p.(fnames{x}),ex.(fnames{x}))
            if(isfield(p,'overrideParameters')) % if 'overrideParameters' is set, then use that value
                override=p.overrideParameters; 
                if(override)  
                    warning(['Using "' fnames{x} '" = "' p.(fnames{x}) '" from passed parameters.']);
                else
                    warning(['Using "' fnames{x} '" = "' ex.(fnames{x}) '" from experiment program.']);
                end
            else                                % otherwise ask specifically for each parameter
                override=(input(['Override "' fnames{x} '": "' p.(fnames{x}) ...
                            '" instead of "' ex.(fnames{x}) '" (1/0) ?'] ))
            end;
        end;
        ex=setfield(ex, fnames{x}, getfield(p, fnames{x}));
    end;
    if isfield(params, 'last')  last=params.last; end;
    if isfield(params,'data')   results=params.data; end;
    if isfield(params,'trials') trials=params.trials; end;
    if isfield(params,'edfFiles') 
        result.edfFiles=params.edfFiles; 
    else
        result.edfFiles={};
    end;
    if isfield(params,'startTimes')
        result.startTimes=params.startTimes;
    else
        result.startTimes={};
    end;
end

if ~isfield(ex,'useEyelink') ex.useEyelink=0;end;
if ~isfield(ex,'useScreen') ex.useScreen=1;end;
if ex.useEyelink, ex.useScreen=1;end; % can't have eyelink without screen
if ~ex.useScreen, scr=0;end;
if ~ex.useEyelink  el=0;end;
if(~exist('result','var')) result.startTimes={};end;
result.startTimes   = {result.startTimes{:}, datestr(now,31)};    
    
%%%%%%%%%%% CREATE TRIALS %%%%%%%%%%%%%%%%%
if exist('trials')~=1,  trials = createTrials(ex); end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


try                       
    if ex.useScreen
        scr=prepareScreen(ex);  % initialise screen (scr struct)
        ex.screenSize=scr.ssz;  
    end;

    datestring=datestr(now,30);
    namebase=datestring([5:8, 10:13]); %base filename = date
    if(ex.useEyelink)       % initialise eyelink (el struct)
        if eyelink('IsConnected') eyelink('Shutdown');end;
        if(ex.useEyelink==1)
            eyelink('initialize');
        else
            eyelink('InitializeDummy');
        end
        eyelink('command', 'calibration_type = HV9');
        eyelink('command', 'saccade_velocity_threshold = 35');
        eyelink('command', 'saccade_acceleration_threshold = 9500');
        eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
        eyelink('command', 'file_sample_data = GAZE,AREA,STATUS');
        eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON');
        eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,AREA');
        eyelink('command', 'screen_pixel_coords = 0,0,%d,%d', scr.ssz(1), scr.ssz(2));
        Eyelink('Command', 'pupil_size_diameter = YES');
        el=EyelinkInitDefaults(scr.w);
        el.file=[namebase '.edf'];
        eyelink('openfile', el.file );
        result.file   = el.file;
        el.disallowEarlySaccades=1;
        el.backgroundcolour=ex.bgColour;
        el.foregroundcolour=ex.fgColour;
        if(~isfield(result,'edfFiles')) result.edfFiles={};end;
        result.edfFiles={result.edfFiles{:}, result.file};
        EyelinkDoTrackerSetup(el);
        FlushEvents;
    end;
    result.trials = trials;     % save trial structure in output
    result.params = ex;
    fatal_error   = 0;

    % Practice trials
    if isfield(ex,'practiceTrials') & prod(last)==1
        prac=createTrials(ex);
        if ex.useScreen
            drawTextCentred(scr, 'Practice trials', ex.fgColour);
            screen('Flip', scr.w);
        end;
        KbWait; 
        for i=1:ex.practiceTrials
            if ex.useEyelink
                eyelink('startrecording');
                el.eye=eyelink('eyeavailable')+1;
                if ~el.eye
                    error('No eye available');
                    el.eye=1;
                end;
                eyelink('message', 'PRACTICE %d',i);
                eyelink('message', 'VOID_TRIAL',i);
            end;
            kcode = 1; while any(kcode) [z z kcode]=KbCheck; end;
            FlushEvents; % ensure no keys pressed at start of trial
            tr = doTrial(scr, el, ex, trials(1+floor(i/ex.blockLen),1+mod(i,ex.blockLen)));
            [z z kcode]=KbCheck;
            if kcode(27) break; end;
            if kcode(112) allowinput(scr,ex); end; % allow modification of expt params
            if tr.key==0  tr.key=find(kcode); end;
            if tr.key==27 tr.R=ex.R_ESCAPE;  end;
            if tr.key=='R' tr.R=ex.R_NEEDS_REPEATING; end;
            if tr.key=='D' dodriftcorrection(el ); tr.R=ex.R_NEEDS_REPEATING;end;
            if tr.key=='C' EyelinkDotrackersetup(el); tr.R=ex.R_NEEDS_REPEATING; end;
            result.practiceResult(i)=tr;
            if ex.useEyelink
                eyelink('stoprecording');
                eyeStatus=eyelink('checkrecording');
                switch(eyeStatus)
                    case el.ABORT_EXPT, fatal_error=1; break;
                    case el.REPEAT_TRIAL, tr.key='R';
                    case el.SKIP_TRIAL,   tr.key='R';
                end;
            end;
            if fatal_error | tr.R==ex.R_ESCAPE; break;end;
        end
    end;
            
            

    % Main blocks and trials
    if ex.useScreen
        screen(scr.w, 'FillRect',ex.bgColour, scr.sszrect);
        drawTextCentred(scr, 'Start of Experiment', ex.fgColour);
        screen('Flip', scr.w);
    else
        disp('Start of experiment - press a key');
    end;
    KbWait; 
    if length(ex.blocks)==1, bnum=last(1):ex.blocks;
    else bnum = ex.blocks(last(1):end);
    end;
    for b=last(1):ex.blocks
        if exist('blockStart') 
            kcode = 1; while any(kcode) [z z kcode]=KbCheck; end;
            FlushEvents '';
            blockStart(scr,el,ex,trials(b,1));
        end;
        for t=1:ex.blockLen
            kcode = 1; while any(kcode) [z z kcode]=KbCheck; end;
            FlushEvents; % ensure no keys pressed at start of trial
            if(b==last(1) & t<last(2)) continue; end;
            tr.key=0; tr.R=0;
            while ~fatal_error & (tr.key=='R' | tr.key==0 | tr.R==ex.R_NEEDS_REPEATING) 
                if ex.useEyelink 
                    eyelink('startrecording'); 
                    el.eye=eyelink('eyeavailable')+1;
                    if ~el.eye 
                        error('No eye available');
                        el.eye=1;
                    end;
                    eyelink('message', 'B %d T %d', b,t);
                end;
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                tr = doTrial(scr, el, ex, trials(b,t));
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                [z z kcode]=KbCheck;
                if kcode(27) tr.key=27;end; %override repeat trial if escape pressed.
                if kcode(112) allowinput(scr,ex); end; % allow modification of expt params
                if tr.key==0  tr.key=find(kcode); end;
                if tr.key==27 tr.R=ex.R_ESCAPE;  end;
                if tr.key=='R' tr.R=ex.R_NEEDS_REPEATING; end;
                if tr.key=='D' dodriftcorrection(el ); tr.R=ex.R_NEEDS_REPEATING;end;
                if tr.key=='C' EyelinkDotrackersetup(el); tr.R=ex.R_NEEDS_REPEATING; end;
                if ex.useEyelink   
                    if(tr.R==ex.R_NEEDS_REPEATING) eyelink('message', 'VOID_TRIAL'); end;
                    eyelink('stoprecording'); 
                    eyeStatus=eyelink('checkrecording');
                    switch(eyeStatus)
                        case el.ABORT_EXPT, tr.R=ex.R_ESCAPE; fatal_error=1; eyelink('stoprecording'); break;
                        case el.REPEAT_TRIAL, tr.key='R';eyelink('stoprecording');
                        case el.SKIP_TRIAL,   tr.key='R';eyelink('stoprecording');
                    end;
                end;
            end; %while trial needs repeating
            if(exist('results')==1)
                [results tr]=ensureStructsAssignable(results,tr);
            end;
            results(b,t) = tr;
            if(tr.R==ex.R_ERROR | tr.R==ex.R_ESCAPE) break; end;
            result.data   = results;
            result.last   = [b,t];
            save 'LastExperiment' result
        end; %end of block
        save([namebase '.mat'], 'result');
        if ex.useScreen
            drawTextCentred(scr, 'End of block', ex.fgColour);
            screen('Flip', scr.w);
        end;
        KbWait;
        [z z kcode]=KbCheck;
        if kcode(27) | fatal_error  break;end;
    end; %end of experiment
catch
    screen closeall;        % restore screen
    save 'errordump';
    if exist('results','var')
        result.data=results;
    end;
    e=lasterror;
    fprintf('Error : %s\n',...
        e.message);
    for(i=1:length(e.stack))
        disp(e.stack(i));
    end
end

screen closeall;        % restore screen
if ex.useEyelink        % close eye data file then transfer
    if(eyelink('isConnected'))
        eyelink('closefile');  
        fprintf('Downloading edf...');
        if eyelink('receivefile',el.file,el.file) < 0
            fprintf('Error in receiveing file!\n'); 
        end;
        fprintf('Done\n');
        eyelink('shutdown');
    end;
end;
FlushEvents '';
ShowCursor;

function allowinput(scr,ex)
%screen('OpenWindow', scr.w, ex.bgColour, [0 0 100,100]);
disp('type "return" to return to experiment, or "dbquit" to end expt');
keyboard();
%Screen('OpenWindow', scr.w, ex.bgColour, [0 0 scr.ssz]);
