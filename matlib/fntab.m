function r=fntab(fun,A, B)
% Function table of func(A,B)
%   fntab ( @func , A, B )
%   fntab ('binop', A, B);
% returns matrix of size [size(A) size(B)] 
% with dimensions 1:ndims(A) representing values in A, and dimensions
% ndims(A)+1:ndims(A)+ndims(B) representing values in B.
% e.g. fntab(@plus,[1 2],[3 4 5]) = [4 5 6; 5 6 7]
% or   fntab('+',A,B)
% 
% UPDATE 2011: I discovered you can do all this with BSXFUN
if(size(A,1)==1) A=A'; end;
if(size(B,1)==1) B=B'; end;
a=repmat(A,[ones(1,ndims(A)) size(B)]);
neworder=[ndims(B)+(1:ndims(A)), 1:ndims(B)];
if(length(neworder)>1)
    B2=permute(B, neworder);
else
    B2=B;
end;
b=repmat(B2,[size(A) ones(1,ndims(B))]);
if ischar(fun)
    r=eval(['a ' fun ' b']);
else
    r=fun(a,b);
end

function r=ndims(A)
r=sum(size(A)>1);