function X=sumall(X)
% Y=sumall(X) - summate across all dimensions, returning a scalar
while prod(size(X))>1
    X=sum(X);
end;
    