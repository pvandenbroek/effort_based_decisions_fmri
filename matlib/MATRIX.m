function MATRIX = nanassign(MATRIX, INDICES, VECTOR)
% set the slice of matrix:
%  if indices is [2 5 nan 7], then do
%  MATRIX(2,5,:,7) = VECTOR
% 
%  extending MATRIX if vector is too long,
%  and extending VECTOR if it is too short

col=find(isnan(INDICES));
if(length(col)>1) error('only one nan allowed at present!');end
if(sum(size(vector)>1)>1) error('vector must be 1-dimensional');end
sm=size(MATRIX,col); sv=length(VECTOR);
if(sm>sv)
  addsize = INDICES;
  addsize(col) = sm-sv;
  MATRIX=cat(col, MATRIX, nan*ones(addsize))
elseif(sv>sm)
  dim=find(size(VECTOR)>1);
  if(dim==[]) dim=1;end % if single elem, doesn't matter which dir!
  VECTOR=cat(dim, VECTOR, nan*ones(sv-sm,1));
end

for(i=1:length(INDICES))
  if(isnan(INDICES(i)))
    subs{i}=':';
  else
    subs{i}=INDICES(i);
  end
end

subsasgn(MATRIX, subs , VECTOR);