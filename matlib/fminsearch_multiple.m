function [x,fval,exitflag,output] = fminsearch_multiple(func, start, N, varargin)
% function fminsearch_multiple(func, start, N, varargin)
% Runs fminsearch N times.
% 
% start is [ min1 min2 min3 ...
%            max1 max2 max3 .... ]
% 
% for each parameter
% 
% we will choose random values of the parameters in the range min-max for
% each call to fminsearch.

minval=inf; minp=mean(start);

for i=1:N
  for j=1:size(start,2)
    p0(j) = start(1,j) + rand*(start(2,j)-start(1,j));
  end
  [x,fval,exitflag,output] = fminsearch(func,p0,varargin{:});
  if fval<minval
    minval=fval;
    minp=x;
    minexit=exitflag;
    minoutput=output;
  end
end
% restore the outputs of the best fitting trial
fval=minval;
x=minp;
output=minoutput;
exitflag=minexit;