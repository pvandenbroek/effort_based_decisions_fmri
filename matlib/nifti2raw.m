% function exportData(img, output, scaling)
% img:     input file (nii format)
% output:  raw filename
% scaling = 1/256 for 16-bit NII
% 
% from Martin Pyka
function exportData(img, output, scaling)
vol = spm_vol(img);
data = spm_read_vols(vol);
data = uint8(data*scaling);
figure, imagesc(data(:,:,50))
fid = fopen(output, 'wb')
fwrite(fid, data, 'uint8')
fclose(fid)