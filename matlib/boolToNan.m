function Y=boolToNan(X)
% convenience method to convert a matrix of booleans to a matrix of 0 or nan
% just replaces all the 'trues' with 'nans'.
% i.e.  X(X) = nan
Y=X*1; % convert to numberic first
Y(X)=nan;