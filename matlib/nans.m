function Y=nans(varargin)
Y=nan*ones(varargin{:});