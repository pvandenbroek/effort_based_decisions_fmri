function conditionalAccuracyPlot(RT,Corr, Nbins, styles, varargin)
% h=conditionalAccuracyPlot(RT,Corr, nbins, styles)
% RT{ SUBJECT, CONDITION } ( TRIAL )
% columns are conditions, plotted as separate lines
% styles is a cell with a string for each condition
%
% note that, since the function uses nanmean() to calculate the accuracy,
% instead of plotting a percentage correct, you could
% remove the transform and use this to plot the mean of any value when 
% binned by RT.


% transform proportions? e.g. arcsine transform or logistic function
% TRANSFORM=@(x) asin(sqrt(x));

if(~exist('Nbins','var')) Nbins=5;end;


if(~exist('styles','var')) 
  colstr='rgbcmyw';
  for(i=1:size(RT,2))
    styles{i}=colstr(mod(i-1,length(colstr))+1);
  end
end

for(i=1:size(RT,1)) % for each subject
  for(j=1:size(RT,2)) % for each condition
    top=-inf;
    for(q=1:Nbins) % bin the RTs into Nbins
      bottom=top;
      if(q<Nbins)   top=quantile(RT{i,j},q/Nbins);
      else          top=inf;
      end
      f=RT{i,j}>bottom & RT{i,j}<top;
      acc(i,j,q)=nanmean( Corr{i,j}(f) );    % mean of Corr
      if exist('TRANSFORM') acc=TRANSFORM(acc); end
      mrt(i,j,q)=quantile(RT{i,j},(q-0.5)/Nbins); % centre (median) RT of the bin
      % alternative: use mean of top and bottom of bin?
    end
  end
end

doErrorBars=0;

oldhold=ishold();
  
for(j=1:size(RT,2))
  if(doErrorBars)
    errorbarxy(  sq(mean(mrt(:,j,:),1)), ...
      sq(mean(acc(:,j,:),1)), ...
      sq(std(mrt(:,j,:),[],1))/sqrt(size(mrt,1)), ...
      sq(std(acc(:,j,:),[],1))/sqrt(size(acc,1)) ...
      ,[],[],styles{j}, .2*[1 1 1]);
  else
    plot(sq(mean(mrt(:,j,:),1)), asin(sqrt( ...
      sq(mean(acc(:,j,:),1)) ...
      )), styles{j});
  end
  hold on
end
if ~ishold
  hold off;
end


